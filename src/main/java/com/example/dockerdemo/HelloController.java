package com.example.dockerdemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HelloController {
    private static final String MSG_ENV_VAR = "DOCKERDEMO_GREETING";
    private static final String DEFAULT_GREETING = "Default Greeting!";

    @Autowired
    private Environment env;

    @GetMapping(value = "/hello")
    public String hello(@RequestParam(value = "name", required = false) String name) {
        log.info("New logging");
        if (name == null || name.length() == 0) {
            log.info("Hello received without name");

            String msg = env.getProperty(MSG_ENV_VAR, DEFAULT_GREETING);
            if (DEFAULT_GREETING.equals(msg)) {
                log.info("Using default greeting. Propably no standard greeting set in environment");
            }

            return msg;
        } else {
            log.info("Hello received from " + name);
            return String.format("Hi %s!", name);
        }
    }
}
