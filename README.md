# dockerdemo

This is a "Hello Word" Spring Boot application to be packaged and executed in a container under Docker / Kupernetes.

## Package
``mvn clean package``

## Test run locally
``java -jar target/dockerdemo-0.0.1-SNAPSHOT.jar``

## Build Docker image
``docker build -t registry.gitlab.com/klaus.kirkelund.andersen/dockerdemo .`` 

## Push Docker image to registry
``docker push registry.gitlab.com/klaus.kirkelund.andersen/dockerdemo``

## Run with Docker
``docker run -p 8080:8080 --rm -dt --name dockerdemo registry.gitlab.com/klaus.kirkelund.andersen/dockerdemo``

## Test running application
``wget localhost:8080/hello``

or

``curl localhost:8080/hello``

or by pointing your browser to 

``localhost:8080/hello``

## Deploy to Kupernetes
OBS! The deploy to Kupernetes using `kubectl apply -f` is deprecated and the files will (most propably) not be updated.  

Instead deploy using helm (see below).
```
kubectl apply -f dockerdemo-namespace.yaml
kubectl apply -f dockerdemo-deployment.yaml
kubectl apply -f dockerdemo-service.yaml
```

## Deploy to Kupernetes using Helm

### Local
``helm install dockerdemo helm/. --set environment=local``
``helm install dockerdemo helm/. --set environment=local --create-namespace -n <namespace> ``

### Lab
``helm install dockerdemo helm/. --set environment=lab -n <namespace>``

## Flux

<img alt="Flux" src="https://avatars.githubusercontent.com/u/52158677?&s=50"/>

### Local k8s

#### Prerequisites

#### Prepare to use Flux
* Install the Flux CLI
* Export your credential
```
export GITLAB_FLUX_TOKEN=<gitlab token>
export GITLAB_FLUX_USER=<gitlab user>
```

* Check your Kubernetes cluster

Refer to https://fluxcd.io/docs/get-started/ for instructions

#### Install Flux onto your cluster
```
flux bootstrap gitlab \
--owner=$GITLAB_FLUX_USER \
--repository=flux-example \
--branch=main \
--path=./clusters/my-cluster \
--personal
```

See https://gitlab.com/Klaus.Kirkelund.Andersen/flux-example

In case the command fails, then you might need to create the actual repository in GitLab first.

#### Install into Flus
```
kubectl create ns dockerdemo
kubectl apply -f flux/gitrepository.yaml -n dockerdemo
kubectl apply -f flux/helmrelease.yaml -n dockerdemo
```

#### A couple of flux commands
```
flux get sources all
flux delete hr dockerdemo
flux logs --all-namespaces
flux logs -f --all-namespaces
```

